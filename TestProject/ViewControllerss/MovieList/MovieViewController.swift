//
//  MovieViewController.swift
//  TestProject
//
//  Created by Vishnu Satheesh  on 06/02/24.
//

import UIKit
import Combine

class MovieViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    private var binding = Set<AnyCancellable>()
    var movieData: [Movie] = [Movie]()
    var viewModel = MovieViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        setupBinding()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        viewModel.getMovieList()
    }
    
    func setupBinding() {
        viewModel.movieSubject.sink { [weak self] response  in
            switch response {
                
            case .success(let movie):
                self?.movieData = movie.movies
                self?.tableView.reloadData()
            case .failure(let error):
                if let errorMessage = error as? APIError {
                    self?.presentAlert(withTitle: "Alert", message: errorMessage.message)
                }
                else {
                    self?.presentAlert(withTitle: "Alert", message: error.localizedDescription)
                }
            }
        }.store(in: &binding)
            
    }

}
extension MovieViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        movieData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "movieCell") as? MovieTableViewCell else { return UITableViewCell()}
        let model = movieData[indexPath.row]
        cell.model = model
        return cell
    }
    
    
    
}
