//
//  MovieTableViewCell.swift
//  TestProject
//
//  Created by Vishnu Satheesh  on 06/02/24.
//

import UIKit
import SDWebImage
class MovieTableViewCell: UITableViewCell {

    @IBOutlet weak var dpImg: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    
    var model: Movie! {
        didSet {
            setupCellModel()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setupCellModel() {
        titleLbl.text = model.title
        dateLbl.text = model.year
        descLbl.text = model.overview
        let urlString = "https://image.tmdb.org/t/p/w300"+(model.posterImage ?? "")
        
        dpImg.sd_setImage(with: URL(string: urlString))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
