//
//  MovieViewModel.swift
//  TestProject
//
//  Created by Vishnu Satheesh  on 06/02/24.
//

import Foundation
import Combine

class MovieViewModel {
    var webservice = WebserviceMangaer(token: nil)
    private var cancellable : AnyCancellable?
    var movieSubject: PassthroughSubject = PassthroughSubject<Result<MoviesData,Error>, Never>()
    
    func getMovieList() {
        let popularMoviesURL = "https://api.themoviedb.org/3/movie/popular?api_key=4e0be2c22f7268edffde97481d49064a&language=en-US&page=1"
        
        guard let url = URL(string: popularMoviesURL) else {return}
        cancellable = webservice.apiRequest(url: url, resultType: MoviesData.self, httpMethodType: .get)
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { [weak self] (completion) in
                switch completion {
                    
                case .finished:
                    print("success")
                case .failure(let error):
                    self?.movieSubject.send(.failure(error))
                }
            }, receiveValue: { [weak self] movies in
                self?.movieSubject.send(.success(movies))
            })
    }
    
}
