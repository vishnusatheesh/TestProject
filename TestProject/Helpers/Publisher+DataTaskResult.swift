//
//  Publisher+DataTaskResult.swift

//
//  Created by Vishnu Satheesh  on 19/12/23.
//

import Foundation
import Combine

typealias DataTaskResult = (data: Data, response: URLResponse)

enum ValidationError: Error {
    case error(Error)
    case jsonError(Data)
}
struct APIError : Decodable,Error {
    var status : String
    var code : Int
    var message : String
}


extension Publisher where Output == DataTaskResult {
    func validateStatusCode(_ isValid: @escaping (Int) -> Bool) -> AnyPublisher<Output, ValidationError> {
        return validateResponse { (data, response) in
            let statusCode = (response as? HTTPURLResponse)?.statusCode ?? -1
            return isValid(statusCode)
        }
    }

    func validateResponse(_ isValid: @escaping (DataTaskResult) -> Bool) -> AnyPublisher<Output, ValidationError> {
        return self
            .mapError { .error($0) }
            .flatMap { (result) -> AnyPublisher<DataTaskResult, ValidationError> in
                let (data, _) = result
                if isValid(result) {
                    return Just(result)
                        .setFailureType(to: ValidationError.self)
                        .eraseToAnyPublisher()
                } else {
                    return Fail(outputType: Output.self, failure: .jsonError(data))
                        .eraseToAnyPublisher()
                }}
            .eraseToAnyPublisher()
    }
    func mapJsonValue<Output: Decodable>(to outputType: Output.Type, decoder: JSONDecoder) -> AnyPublisher<Output, Error> {
        return self
            .map(\.data)
            .decode(type: outputType, decoder: decoder)
            .eraseToAnyPublisher()
    }
}
extension Publisher where Failure == ValidationError {
    func mapJsonError<E: Error & Decodable>(to errorType: E.Type, decoder: JSONDecoder) -> AnyPublisher<Output, Error> {
        return self
            .catch { (error: ValidationError) -> AnyPublisher<Output, Error> in
                switch error {
                case .error(let e):
                    return Fail(outputType: Output.self, failure: e)
                        .eraseToAnyPublisher()
                case .jsonError(let d):
                    return Just(d)
                        .decode(type: E.self, decoder: decoder)
                        .flatMap { Fail(outputType: Output.self, failure: $0) }
                        .eraseToAnyPublisher() } }
            .eraseToAnyPublisher()
    }
}
extension URLSession {
    func dataTaskPublisher<Output: Decodable>(for request: URLRequest) -> AnyPublisher<Output, Error> {
        return self
            .dataTaskPublisher(for: request)
            .validateStatusCode({ (200..<300).contains($0) })
            .mapJsonError(to: APIError.self, decoder: JSONDecoder())
            .mapJsonValue(to: Output.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
}

