//
//  WebserviceManager.swift

//
//  Created by Vishnu Satheesh  on 19/12/23.
//

import Foundation
import Combine

struct WebserviceMangaer {
    
    
    private var _token: String? = nil
    
    init(token: String?) {
        _token = token
    }
    
    enum httpMethods :String {
        case get = "GET"
        case put = "PUT"
        case post = "POST"
        case delete = "DELETE"
        case patch = "PATCH"
    }
    
    enum HTTPError: LocalizedError {
        case statusCode
    }
    
    func apiRequest <T:Decodable>(url: URL,resultType: T.Type,httpMethodType: httpMethods,requestBody: Data? = nil)-> AnyPublisher<T, Error>{
        switch httpMethodType {
        case .get:
            return getData(requestUrl: url, resultType: resultType)
            //             break
        case .post:
            return postData(requestUrl: url, requestBody: requestBody, resultType: resultType)
            //            break
        case .put:
            return putData(requestUrl: url, resultType: resultType, requestBody: requestBody)
            //            break
        case .delete:
            return deleteData(requestUrl: url, resultType: resultType)
            //            break
            
        case .patch:
            return patchData(requestUrl: url, requestBody: requestBody, resultType: resultType)
        }
        
    }
    // MARK: - GET Api
    
    private func getData<T:Decodable>(requestUrl: URL, resultType: T.Type)->AnyPublisher<T, Error>
    {
        var urlRequest = createUrlRequest(requestUrl: requestUrl)
        urlRequest.httpMethod = "get"
        urlRequest.addValue("application/json", forHTTPHeaderField: "content-type")
        urlRequest.addValue("user", forHTTPHeaderField:"accessLevel")
        urlRequest.addValue("iphone", forHTTPHeaderField: "platform")
        
        return performOperation(requestUrl: urlRequest, responseType: resultType)
    }
    
    // MARK: - POST Api
    private func postData<T:Decodable>(requestUrl: URL, requestBody: Data?, resultType: T.Type)->AnyPublisher<T, Error>
    {
        var urlRequest = createUrlRequest(requestUrl: requestUrl)
        urlRequest.httpMethod = "post"
        if let body = requestBody {
            urlRequest.httpBody = body
        }
        
        urlRequest.addValue("application/json", forHTTPHeaderField: "content-type")
        urlRequest.addValue("user", forHTTPHeaderField:"accessLevel")
        urlRequest.addValue("iphone", forHTTPHeaderField: "platform")
        
        
        return performOperation(requestUrl: urlRequest, responseType: resultType)
    }
    
    //MARK: - POST Multipart
    
    //     func postMultiPart<T:Decodable>(requestUrl: URL, requestBody: ImageUpload, resultType: T.Type)->AnyPublisher<T,Error>
    //    {
    //        var urlRequest = createUrlRequest(requestUrl: requestUrl)
    //        let lineBreak = "\r\n"
    //        urlRequest.httpMethod = "post"
    //        let boundry = "--------------------------------\(UUID().uuidString)"
    //        urlRequest.setValue("multipart/form-data; boundary=\(boundry)", forHTTPHeaderField: "Content-Type")
    //        urlRequest.addValue("user", forHTTPHeaderField:"accessLevel")
    //        urlRequest.addValue("iphone", forHTTPHeaderField: "platform")
    //
    //        var requestData = Data()
    //        requestData.append("--\(boundry)\r\n" .data(using: .utf8)!)
    //        requestData.append("Content-Disposition: form-data; name=\"image\"; filename=\"\(requestBody.filename).png\"\r\n" .data(using: .utf8)!)
    //        requestData.append("Content-Type: image/png\(lineBreak + lineBreak)" .data(using: .utf8)!)
    //        let imagedata = requestBody.image.pngData()!
    //
    //        requestData.append(imagedata)
    //        requestData.append(lineBreak .data(using: .utf8)!)
    //        requestData.append("--\(boundry)--\r\n" .data(using: .utf8)!)
    //
    //       // urlRequest.addValue("\(requestData.count)", forHTTPHeaderField: "content-length")
    //        urlRequest.httpBody = requestData
    //
    //        return performOperation(requestUrl: urlRequest, responseType: resultType)
    //    }
    // MARK: - PATCH Api
    
    private func patchData<T:Decodable>(requestUrl: URL, requestBody: Data?, resultType: T.Type)->AnyPublisher<T, Error>
    {
        var urlRequest = createUrlRequest(requestUrl: requestUrl)
        urlRequest.httpMethod = "PATCH"
        if let body = requestBody {
            urlRequest.httpBody = body
        }
        
        urlRequest.addValue("application/json", forHTTPHeaderField: "content-type")
        urlRequest.addValue("user", forHTTPHeaderField:"accessLevel")
        urlRequest.addValue("iphone", forHTTPHeaderField: "platform")
        
        
        return performOperation(requestUrl: urlRequest, responseType: resultType)
    }
    
    //    func putProfileImageData<T:Decodable>(requestUrl: URL, resultType: T.Type, requestBody: profileImage)->AnyPublisher<T, Error>
    //    {
    //        var urlRequest = createUrlRequest(requestUrl: requestUrl)
    //        let lineBreak = "\r\n"
    //        urlRequest.httpMethod = "put"
    //        let boundry = "--------------------------------\(UUID().uuidString)"
    //        urlRequest.setValue("multipart/form-data; boundary=\(boundry)", forHTTPHeaderField: "Content-Type")
    //        urlRequest.addValue("user", forHTTPHeaderField:"accessLevel")
    //        urlRequest.addValue("iphone", forHTTPHeaderField: "platform")
    //
    //        var requestData = Data()
    //        requestData.append("--\(boundry)\r\n" .data(using: .utf8)!)
    //        requestData.append("Content-Disposition: form-data; name=\"profile_image\"; filename=\"\(requestBody.name).png\"\r\n" .data(using: .utf8)!)
    //        requestData.append("Content-Type: image/png\(lineBreak + lineBreak)" .data(using: .utf8)!)
    //        let imagedata = requestBody.image.pngData()!
    //
    //        requestData.append(imagedata)
    //        requestData.append(lineBreak .data(using: .utf8)!)
    //        requestData.append("--\(boundry)--\r\n" .data(using: .utf8)!)
    //
    //        urlRequest.httpBody = requestData
    //        return performOperation(requestUrl: urlRequest, responseType: resultType)
    //    }
    
    // MARK: - PUT Api
    private func putData<T:Decodable>(requestUrl: URL, resultType: T.Type, requestBody: Data?)->AnyPublisher<T, Error>
    {
        var urlRequest = createUrlRequest(requestUrl: requestUrl)
        urlRequest.httpMethod = "put"
        if let body = requestBody {
            urlRequest.httpBody = body
        }
        urlRequest.addValue("application/json", forHTTPHeaderField: "content-type")
        urlRequest.addValue("user", forHTTPHeaderField:"accessLevel")
        urlRequest.addValue("iphone", forHTTPHeaderField: "platform")
        
        return performOperation(requestUrl: urlRequest, responseType: resultType)
    }
    // MARK: - DELETE Api
    private func deleteData<T:Decodable>(requestUrl: URL, resultType: T.Type)->AnyPublisher<T, Error>
    {
        var urlRequest = createUrlRequest(requestUrl: requestUrl)
        urlRequest.httpMethod = "delete"
        
        return performOperation(requestUrl: urlRequest, responseType: resultType)
    }
    
    private func performOperation<T: Decodable>(requestUrl: URLRequest, responseType: T.Type)->AnyPublisher<T, Error>
    {
        
        return URLSession.shared.dataTaskPublisher(for: requestUrl)
        
    }
    
    private func createUrlRequest(requestUrl: URL) -> URLRequest
    {
        var urlRequest = URLRequest(url: requestUrl)
        urlRequest.timeoutInterval = 30
        if(_token != nil) && (_token?.count ?? 0 > 0)
        {
            let token = "Token \(_token!)"
            print("Token: \(_token!)")
            // urlRequest.addValue(token, forHTTPHeaderField: "X-authorization")
            urlRequest.setValue(token, forHTTPHeaderField: "authorization")
        }
        
        return urlRequest
    }
    
}
